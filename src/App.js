// @flow

import React, { type Node } from 'react';
import logo from './logo.svg';
import { Provider, Container, Subscribe, extend } from './unstated'

import './App.css';

type State = {}
type Props = {}

extend((container) => {
  let setState = container.setState;
debugger
  container.setState = (updater, callback) => {
    console.log(updater, callback)
    return setState
  }
})

class Example extends React.Component<Props, State> {

  state: State;

  constructor(...props) {
    super(props)

    this.state = {
      uno: 0
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>

        <Subscribe to={ [CounterContainer] }>
          {
            (counter) => {
              return (
                <p>State: { counter.state.counter }</p>
              )
            }
          }
        </Subscribe>
      </div>
    )
  }
}

class CounterContainer extends Container {

  store = {
    name:'counter',
    persist: {
      version: 1.0
    }
  }

  state = {
    counter: 1
  }

  increment() {
    this.setState({
      counter: this.state.counter + 1
    })
  }

}


const containers = [new CounterContainer]

class TodoContainer extends Container {

  store = {
    name:'tasks',
    persist: {
      version: 1.0
    }
  }

  state = {
    tasks: []
  }

  add() {
    this.setState({
      tasks: this.state.tasks.concat([ {
        task: 'Task ' + this.state.tasks.length + 1
       } ])
    })
  }

}


function App() {
  return (
    <Provider inject={ containers }>
      <Subscribe to={ [CounterContainer, TodoContainer] }>
        {
          (counter, todo) => {
            return (
              <div>
                <Example />
                <p>State: { counter.state.counter }</p>
                <button onClick={ () => counter.increment() }>Inc</button>
              </div>
            )
          }
        }
      </Subscribe>
    </Provider>
  );
}

export default App;
